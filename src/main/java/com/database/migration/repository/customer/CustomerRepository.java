package com.database.migration.repository.customer;


import com.database.migration.constants.CommonConstants;
import com.database.migration.destinationdata.lookups.Country;
import com.database.migration.destinationdata.lookups.CustomerType;
import com.database.migration.destinationdata.lookups.rowmappers.CountryRowMapper;
import com.database.migration.destinationdata.lookups.rowmappers.CustomerTypeRowMapper;
import com.database.migration.destinationdata.lookups.rowmappers.DestBranchRowMapper;
import com.database.migration.sourcedata.Customer;
import com.database.migration.destinationdata.DestinationCust;
import com.database.migration.repository.BaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public class CustomerRepository extends BaseRepository<Customer, DestinationCust> {


    @Autowired
    public CustomerRepository(JdbcTemplate sourceJdbcTemplate, @Qualifier(CommonConstants.DESTINATION_JDBC_TEMPLATE_BEAN_NAME)
            JdbcTemplate destinationJdbcTemplate) {
        super(sourceJdbcTemplate, destinationJdbcTemplate);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Customer> retrieveDataFromSourceDatabase(int startFrom) {
        String sql = "select * from CUSTOMER where id >= " + startFrom + " order by id";
        List<Customer> customers = super.getSourceJdbcTemplate().query(sql, new CustomerRowMapper());
        if (customers == null) {
            return new ArrayList<>();
        }
        return customers;
    }

    @Override
    public boolean saveDataToDestinationDatabase(List<DestinationCust> migrationData) {
        try{

            String sql = "insert into CUSTOMER (ID," +
                    "ADDRESS," +
                    "BVN," +
                    "CITY," +
                    "CORRES_LANG," +
                    "COUNTRY_OF_RESIDENCE," +
                    "COUNTRY_OF_RISK," +
                    "CUSTOMER_NUMBER," +
                    "CUSTOMER_TYPE," +
                    "EMAIL_ADDRESS," +
                    "FULL_NAME," +
                    "HOME_BRANCH," +
                    "MAIL_DELIVERY_BRANCH," +
                    "MANAGER_ID," +
                    "NATIONALITY," +
                    "PHONE_NUMBER," +
                    "RC_NO_OR_NAT_ID_NO," +
                    "ROUTING_NO," +
                    "SHORT_NAME," +
                    "STATUS," +
                    "SWIFT_BIC,"+
                    "TAX_ID_NO,"+
                    "TITLE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            super.getDestinationJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement pStmt, int branchIndx) throws SQLException {
                    int parameterIndx = 1;
                    pStmt.setLong(parameterIndx++,migrationData.get(branchIndx).getId());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getAddress());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getBvn());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getCity());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getCorrespondenceLanguage());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getCountryOfResidenceCode());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getCountryOfRiskCode());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getCustomerNumber());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getCustomerTypeCode());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getEmailAddress());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getFullName());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getHomeBranchCode());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getMailDeliveryBranchCode());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getManagerId());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getNationality());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getPhoneNumber());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getRcNoOrNatIdNo());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getRoutingNumber());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getShortName());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getStatus());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getSwiftBicCode());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getTaxIdNo());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getTitle());
                }

                @Override
                public int getBatchSize() {
                    return migrationData.size();
                }
            });
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

    }

    public String findCountryCode(String countryOfResidenceCode) {
        String sql = "select NUMERIC_CODE from COUNTRY where ISOCODE = " + "'" + countryOfResidenceCode + "'";
        List<Country> country = super.getDestinationJdbcTemplate().query(sql, new CountryRowMapper());
        return country.get(0).getNumericCode();
    }

    public Long getCustomerTypeCode(String customerTypeCode) {
        String sql = "select id from CUSTOMER_TYPE where SUB_CAT = " + "'" + customerTypeCode + "'";
        List<CustomerType> customerCode = super.getDestinationJdbcTemplate().query(sql, new CustomerTypeRowMapper());
        return customerCode.get(0).getId();
    }
}

