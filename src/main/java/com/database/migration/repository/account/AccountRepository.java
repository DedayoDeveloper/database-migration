package com.database.migration.repository.account;


import com.database.migration.codes.AccountTypeCode;
import com.database.migration.constants.CommonConstants;
import com.database.migration.destinationdata.DestinationBranch;
import com.database.migration.destinationdata.lookups.AccountType;
import com.database.migration.destinationdata.lookups.Currency;
import com.database.migration.destinationdata.lookups.rowmappers.AccountTypeRowMapper;
import com.database.migration.destinationdata.lookups.rowmappers.CurrencyRowMapper;
import com.database.migration.destinationdata.lookups.rowmappers.DestBranchRowMapper;
import com.database.migration.sourcedata.Account;
import com.database.migration.destinationdata.DestinationAcct;
import com.database.migration.repository.BaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository
public class AccountRepository extends BaseRepository<Account, DestinationAcct> {

    private static final Logger logger = LoggerFactory.getLogger(AccountRepository.class);

    @Autowired
    public AccountRepository(JdbcTemplate sourceJdbcTemplate, @Qualifier(CommonConstants.DESTINATION_JDBC_TEMPLATE_BEAN_NAME)
            JdbcTemplate destinationJdbcTemplate) {
        super(sourceJdbcTemplate, destinationJdbcTemplate);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Account> retrieveDataFromSourceDatabase(int startFrom) {
        String sql = "select * from ACCOUNT where id >= " + startFrom + " order by id";
        List<Account> accounts = super.getSourceJdbcTemplate().query(sql,new AccountRowMapper());
        if (accounts == null) {
            return new ArrayList<>();
        }
        return accounts;
    }



    @Override
    public boolean saveDataToDestinationDatabase(List<DestinationAcct> migrationData) {
        try{

            String sql = "insert into ACCOUNT (ID," +
                    "ACCOUNT_NUMBER," +
                    "ACCOUNT_NAME," +
                    "ACCOUNT_TYPE," +
                    "BRANCH," +
                    "CURRENCY," +
                    "CUSTOMER_NUMBER," +
                    "DATE_OPENED," +
                    "EFFECTIVE_DATE," +
                    "MULTI_CURRENCY_ACCT," +
                    "SHORT_NAME," +
                    "INTERNATIONAL_ACCT_NO," +
                    "STATUS) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            super.getDestinationJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement pStmt, int branchIndx) throws SQLException {
                    int parameterIndx = 1;
                    pStmt.setLong(parameterIndx++,migrationData.get(branchIndx).getId());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getAccountNumber());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getFullName());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getAccountTypeCode());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getBranchCode());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getCurrency());
                    pStmt.setLong(parameterIndx++, migrationData.get(branchIndx).getCustomerCode());
                    pStmt.setDate(parameterIndx++, new java.sql.Date(migrationData.get(branchIndx).getDateOpened().getDate()));
                    pStmt.setDate(parameterIndx++,  new java.sql.Date(migrationData.get(branchIndx).getEffectiveDate().getDate()));
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getMultiCurrency());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getShortName());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getIban());
                    pStmt.setString(parameterIndx++, migrationData.get(branchIndx).getStatus());
                }

                @Override
                public int getBatchSize() {
                    return migrationData.size();
                }
            });
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

    }

    public String currencyLookUp(String currency) {
        String sql = "select CURRENCY_NUMBER from CURRENCY where CODE = " + "'" + currency + "'";
        List<Currency> id = super.getDestinationJdbcTemplate().query(sql, new CurrencyRowMapper());
        return id.get(0).getCurrencyNumber();
    }

    public Long retrieveAccountTypeCode(AccountTypeCode destAcctCode){
        Long newId = null;
        String sql = "select id from ACCOUNTTYPE where CODE = " + "'" + destAcctCode.toString() + "'";
        List<AccountType> id = super.getDestinationJdbcTemplate().query(sql, new AccountTypeRowMapper());
        return id.get(0).getId();
    }

    public Long findBranchId(String branchCode) {
        String sql = "select id from BRANCH where CODE = " + "'" + branchCode + "'";
        List<DestinationBranch> id = super.getDestinationJdbcTemplate().query(sql, new DestBranchRowMapper());
        return id.get(0).getId();
    }



}
