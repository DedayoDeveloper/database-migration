package com.database.migration.repository.branch;

import com.database.migration.sourcedata.Branch;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BranchRowMapper implements RowMapper<Branch> {


    @Override
    public Branch mapRow(ResultSet resultSet, int i) throws SQLException {
        Branch branch = new Branch();
        branch.setCollationOfficeFlag(resultSet.getBoolean("COLLATION_OFFICE_FLAG"));
        branch.setHeadOfficeFlag(resultSet.getBoolean("HEAD_OFFICE_FLAG"));
        branch.setRegionalOfficeFlag(resultSet.getBoolean("REGIONAL_OFFICE_FLAG"));
        branch.setAddress(resultSet.getString("ADDRESS"));
        branch.setName(resultSet.getString("NAME"));
        branch.setCode(resultSet.getString("CODE"));
        branch.setEmailAddress(resultSet.getString("EMAIL_ADDRESS"));
        branch.setCity(resultSet.getString("CITY"));
        branch.setRegionCode(resultSet.getString("REGION_CODE"));
        branch.setPhoneNumber(resultSet.getString("PHONE_NUMBER"));
        branch.setZip(resultSet.getString("ZIP"));
        return branch;
    }

    public BranchRowMapper() {
    }
}
