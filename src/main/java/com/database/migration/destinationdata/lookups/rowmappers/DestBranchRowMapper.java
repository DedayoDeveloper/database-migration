package com.database.migration.destinationdata.lookups.rowmappers;

import com.database.migration.destinationdata.DestinationBranch;
import com.database.migration.sourcedata.Branch;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DestBranchRowMapper implements RowMapper<DestinationBranch> {


    @Override
    public DestinationBranch mapRow(ResultSet resultSet, int i) throws SQLException {
        DestinationBranch branch = new DestinationBranch();
        branch.setId(resultSet.getLong("ID"));
        return branch;
    }
}
