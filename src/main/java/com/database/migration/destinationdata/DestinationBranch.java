package com.database.migration.destinationdata;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class DestinationBranch {


    private Long Id;
    private String zip;
    private String address;
    private String city;
    private String code;
    private String collationOfficeFlag;
    private Long consistencyVersion;
    private String emailAddress;
    private String headOfficeFlag;
    private String name;
    private String phoneNumber;
    private String regionalOfficeFlag;
    private Long regionCode;
    private Long swiftBicCode;

}
