package com.database.migration.destinationdata;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class DestinationAcct {

    private Long id;
    private String fullName;
    private String accountNumber;
    private Long consistencyVersion;
    private String coreBkAcctNo;
    private Date dateOpened;
    private Date effectiveDate;
    private String iban;
    private String multiCurrency;
    private String shortName;
    private String status;
    private Long accountTypeCode;
    private Long branchCode;
    private Long currency;
    private Long customerCode;



}
