package com.database.migration.sourcedata;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
//@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String firstName;
    private String lastName;
    private String loginUserId;
    private String email;
    private String employeeNumber;
    private String branchCode;
    private String secPolicyName;
    private String roleName;

}
