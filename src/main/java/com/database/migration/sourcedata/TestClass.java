package com.database.migration.sourcedata;


import lombok.Data;

@Data
public class TestClass {

    private String id;
    private String userName;
    private String lastName;
    private String email;
    private String address;
    private String money;

}
