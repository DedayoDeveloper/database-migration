package com.database.migration.sourcedata;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Branch {

    private Long Id;
    private String address;
    private String city;
    private String code;
    private boolean collationOfficeFlag;
    private String emailAddress;
    private boolean headOfficeFlag;
    private String name;
    private String phoneNumber;
    private String regionCode;
    private boolean regionalOfficeFlag;
    private String swiftBicCode;
    private String zip;





//    private String branchZone;

}
