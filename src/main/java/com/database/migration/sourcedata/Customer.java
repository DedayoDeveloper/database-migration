package com.database.migration.sourcedata;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Customer {

    private Long id;
    private String address;
    private String bvn;
    private String city;
    private String correspondenceLanguage;
    private String countryOfResidenceCode;
    private String countryOfRiskCode;
    private String customerNumber;
    private String customerTypeCode;
    private String emailAddress;
    private String fullName;
    private String homeBranchCode;
    private String industry;
    private String mailDeliveryBranchCode;
    private String managerId;
    private String nationality;
    private String phoneNumber;
    private String rcNoOrNatIdNo;
    private String routingNumber;
    private String shortName;
    private String status;
    private String swiftBicCode;
    private String taxIdNo;
    private String title;
}
